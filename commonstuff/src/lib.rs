use std::io;

pub fn leave_console_open()
{
    // keep the console open so the user can review the logs
    io::stdin().read_line(
        &mut String::new()
    ).unwrap();
}