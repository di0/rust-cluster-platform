use std::thread;
use std::net::{TcpListener, TcpStream, Shutdown};
use std::io::{Read, Write};
//use commonstuff;


fn main() 
{
    // bootstrap banner
    println!("Rust Cluster Platform!");

    // TODO: DEBUG REMOVE
    another_function();
    let number = yet_another_function();
    println!("{}", number);

    // start server
    start_server();

    commonstuff::leave_console_open();
}

fn another_function() {
    let message = "I'm a message in another function";
    println!("{}", message);
}

fn yet_another_function() -> u64 {
    let number = 2;
    let another_number = 5;
    number + another_number
}

fn handle_client(mut stream: TcpStream) {
    let mut data = [0 as u8; 50]; // using 50 byte buffer
    while match stream.read(&mut data) {
        Ok(size) => {
            // echo everything!
            stream.write(&data[0..size]).unwrap();
            true
        },
        Err(_) => {
            println!("An error occurred, terminating connection with {}", stream.peer_addr().unwrap());
            stream.shutdown(Shutdown::Both).unwrap();
            false
        }
    } {}
}

fn start_server()
{
    let listener = TcpListener::bind("0.0.0.0:3333").unwrap();
    // accept connections and process them, spawning a new thread for each one
    println!("Server listening on port 3333");
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New connection: {}", stream.peer_addr().unwrap());
                thread::spawn(move|| {
                    // connection succeeded
                    handle_client(stream)
                });
            }
            Err(e) => {
                println!("Error: {}", e);
                /* connection failed */
            }
        }
    }
    // close the socket server
    drop(listener);
}

#[test]
fn should_fail() {
    unimplemented!();
}