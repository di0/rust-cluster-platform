please see: 

https://github.com/ZenLulz/rust-multipackages-template

https://doc.rust-lang.org/cargo/reference/manifest.html#the-project-layout


|-- Cargo.lock (Cargo.lock is only located at the root level)
|-- Cargo.toml (Cargo file to set up the workspace)
|-- LICENSE
|-- README.md
|-- example_app (executable package; depends on example_enums)
|   |-- Cargo.toml
|   `-- src
|       `-- main.rs
|-- example_enums (library package; depends on example_maths)
|   |-- Cargo.toml
|   |-- src
|   |   |-- conversions.rs
|   |   `-- lib.rs
|   `-- tests (integration test)
|       `-- integration_tests.rs
`-- example_maths (library package)
|   |-- Cargo.toml
|   `-- src
|       |-- bin (a folder that can contains executable crates)
|       |   `-- multiply_demo.rs (an executable embedded in the package)
|       `-- lib.rs (source + unit tests)
`-- target (the binaries are built at the root level)